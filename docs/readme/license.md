## License

Copyright © {{ copyright }} [{{ company_name }}]({{ website.homepage }}). This project is [{{ license }}]({{ repository.group.python }}/{{ SLUG }}/-/raw/master/LICENSE) licensed.
