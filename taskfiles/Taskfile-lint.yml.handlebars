---
version: '3'

vars:
  IGNORE_FOLDERS: -name .autodoc -o -name .cache -o -name .common -o -name .git -o -name .husky
    -o -name .modules -o -name .npm -o -name .task -o -name .venv -o -name .vscode -o -name node_modules
    -o -name venv

tasks:
  all:
    desc: Lint the project by running all the linters in parallel
    deps:
      - formatting
{{#if hbs.ansible}}
      - ansible
{{/if}}
{{#if hbs.dockerfile}}
      - docker
{{/if}}
      - markdown
{{#if hbs.packer}}
      - packer
{{/if}}
      - private-keys
{{#if hbs.python}}
      - python
{{/if}}
      - scripts
{{#if hbs.vagrant}}
      - vagrant
{{/if}}
{{#if hbs.yaml}}
      - yaml
{{/if}}

{{#if hbs.ansible}}
  ansible:
    deps:
      - :ansible:symlink
      - :common:python-requirements
    desc: Lint Ansible projects using Ansible Lint
    summary: |
      # Lint an Ansible project using Ansible Lint

      This task lints the project using Ansible Lint which will scan the project and report design
      patterns that can possibly be improved. It can be used on both playbooks and roles. The
      configuration for Ansible Lint is stored in the root of the project in a file titled
      `.ansible-lint`. This configuration file is shared between all of our Ansible projects
      so any changes to it need to be made to the upstream Ansible common file repository. Because of
      this, it probably makes more sense to to disable rules (when absolutely necessary) using the
      [syntax described in this link](https://ansible-lint.readthedocs.io/en/latest/rules.html#false-positives-skipping-rules).

      For more information, see [Ansible Lint's GitHub page](https://github.com/ansible-community/ansible-lint).
    cmds:
      - ansible-lint
    sources:
      - "**/*.yml"

{{/if}}
  commit:
    deps:
      - :common:nodejs-dependencies
      - :npm:commitlint
    cmds:
      - |
        if [ ! -z "\{{.CLI_ARGS}}" ]; then
          commitlint -e "\{{.CLI_ARGS}}" --config .common/commitlint.config.cjs
        else
          true warn "The 'lint:commit' task requires a COMMIT_EDITMSG file path passed in as a CLI argument."
        fi
    status:
      - '[[ -z "\{{.CLI_ARGS}}" ]]'

{{#if hbs.dockerfile}}
  docker:
    deps:
      - :software:docker
    desc: Lint Dockerfiles using Hadolint (requires Docker)
    summary: |
      # Lint Dockerfiles using Hadolint

      Hadolint is a linter for Dockerfiles. This task uses Hadolint to reportrue warnings
      and suggestions so that the project is using best practices and is less likely to
      have errors. The task uses Docker to run an ultra-compact container that includes
      Hadolint. Installing Hadolint is done at runtime. The task scans for all files that
      are named either `Dockerfile` or `Dockerfile.j2`. On top of reporting suggestions
      for adhereing to Docker best-practices, Hadolint also leverages Shellcheck to report
      possible errors in the shell logic used by the Dockerfile.

      **If the Dockerfile is named something other than `Dockerfile` or `Dockerfile.j2`, you
      can manually run Hadolint by running:**
      `docker run -v "$PWD:/work" -w /work megabytelabs/hadolint:slim CustomDockerfileName`

      **This task will ignore files in the following directories (and possibly more):**
        * .cache        * .husky        * node_modules
        * .common       * .modules      * test
        * .git          * .task         * venv

      **Example scanning the whole project:**
      `task lint:docker`

      **Example scanning single file:**
      `task lint:docker -- CustomDockerfile`

      For more information, see [Hadolint's GitHub page](https://github.com/hadolint/hadolint).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f \( -name Dockerfile -o
            -name Dockerfile.j2 \) -print0 | xargs -0 -r -n1 docker run -v "$PWD:/work" -w /work
            megabytelabs/hadolint:slim
        else
          docker run -v "$PWD:/work" -w /work megabytelabs/hadolint:slim \{{.CLI_ARGS}}
        fi
    sources:
      - "**/(Dockerfile|Dockerfile.j2)"

{{/if}}
  formatting:
    deps:
      - :npm:prettier
    desc: Lint formatting using Prettier
    summary: |
      # Report formatting errors with Prettier

      This task will run Prettier on the project and list the possible fixes without automatically
      applying the fixes. It will report mistakes like inconsistent indent lengths, trailing spaces,
      and more. This task is a wrapper for the `npm run lint:prettier` command. It will use the configuration
      specified in the `package.json` file under the `prettier` key.

      If this command is incompatible with a file then you can add the file to the `.prettierignore`
      file.

      For more information, see [Prettier's website](https://prettier.io/).
    cmds:
      - prettier --list-different .
      - prettier-package-json --list-different package.json
    sources:
      - "**/*.(css|html|js|json|less|md|mjml|php|scss|sh|ts|xml|yml)"

  lint-staged:
    deps:
      - :common:nodejs-dependencies
      - :common:nodejs-global-dependencies
      - :common:python-requirements
    cmds:
      - lint-staged

  gitleaks:
    deps:
      - :software:gitleaks
    desc: Scans repository (including git history) for possible leaked keys
    summary: |
      # Scan repository with Gitleaks

      Find accidentally committed passwords, private keys, and API keys by scanning the repository with
      Gitleaks.

      **Example of scanning current repository:**
      `task lint:gitleaks`

      **Example of scanning a public git repository:**
      `task lint:gitleaks -- https://github.com/ProfessorManhattan/Windows12`

      For more information, see the [Gitleaks GitHub page](https://github.com/zricethezav/gitleaks).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          gitleaks -p . -v
        else
          gitleaks --repo-url "\{{.CLI_ARGS}}" -v
        fi

  markdown:
    deps:
      - :nodejs:dependencies
      - :npm:remark
    desc: Lint markdown files
    summary: |
      # Lint markdown files

      This task uses `remark-lint` under the hood to provide markdown style recommendations.

      **Example scanning all markdown files:**
      `task lint:markdown`

      **Example scanning single markdown file:**
      `task lint:markdown -- TEST.md`

      For more information on `remark`, see the [GitHub page](https://github.com/remarkjs/remark).

      For more information on `remark-lint`, see the [GitHub page](https://github.com/remarkjs/remark-lint).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f \( -name '*.md' \) -print0 |
            xargs -0 -r -n1 remark
        else
          remark \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*.md"

  markdown-broken-links:
    deps:
      - :npm:markdown-link-check
    desc: Scan markdown files for broken links
    summary: |
      # Report any broken links in the files that end with .md

      This task uses the npm package called `markdown-link-check` to scan all the links
      and then report which ones are broken.

      **Example scanning the whole project:**
      `task lint:markdown-broken-links`

      **Example scanning single file:**
      `task lint:markdown-broken-links -- filename.md`

      For more information on `markdown-link-check`, see their [GitHub page](https://github.com/tcort/markdown-link-check).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f \( -name '*.md' \) -print0 |
            xargs -0 -r -n1 markdown-link-check
        else
          markdown-link-check \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*.md"

{{#if hbs.packer}}
  packer:
    deps:
      - :software:packer
    desc: Validate the Packer templates ending with `template.json`
    summary: |
      # Validate Packer templates

      This task will loop through all the Packer templates ending with `template.json`
      in the root of this project and report any errors that the templates might have.
      Alternatively, you can scan a single file (see example below).

      **Example scanning for all files ending with `template.json` in the root directory:**
      `task lint:packer`

      **Example scanning single file:**
      `task lint:packer -- filename.json`

      For more information on `packer validate`, see the [Packer website](https://www.packer.io/docs/commands/validate).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          for TEMPLATE in *template.json; do
            packer validate "$TEMPLATE"
          done
        else
          packer validate \{{.CLI_ARGS}}
        fi
    sources:
      - "*template.json"

{{/if}}
  private-keys:
    deps:
      - :common:python-requirements
    desc: Scan for private keys
    summary: |
      # Scan for private keys

      This task will scan the project for private keys that might not belong where they are. You
      can pass this task a single file or let it loop through the project. If you loop through
      the project, common folders like 'node_modules/' and 'venv/' will be ignored.

      **Example scanning the whole project:**
      `task lint:private-keys`

      **Example scanning single file:**
      `task lint:private-keys -- filename.ext`
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f -print0 | xargs -0 -r -n1 detect-private-key
        else
          detect-private-key \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*"

  prose:
    deps:
      - :common:python-requirements
    desc: Lint for English prose
    summary: |
      # Lint for English prose

      This task uses Proselint to analyze markdown files for prose. It will generate recommendations
      based on typography, grammar, and wording.

      **Example scanning all markdown files:**
      `task lint:prose`

      **Example scanning specific file (markdown or not):**
      `task lint:prose -- myfile.js`

      For more information, see [Proselint's GitHub page](https://github.com/amperser/proselint).
    cmds:
      - |
        if [ -f ~/.config/proselint/config ]; then
          true info "Backing up '~/.config/proselint/config' to '~/.config/proselint/config.installdoc.backup'"
          mv ~/.config/proselint/config ~/.config/proselint/config.installdoc.backup
        fi
      - mkdir -p ~/.config/proselint
      - cp .common/proselint.json ~/.config/proselint/config
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f \( -name '*.md' \) -print0 |
            xargs -0 -r -n1 proselint
        else
          proselint \{{.CLI_ARGS}}
        fi
      - if [ -f ~/.config/proselint/config.installdoc.backup ]; then
          true info "Restoring pre-existing Proselint configuration"
          mv ~/.config/proselint/config.installdoc.backup ~/.config/proselint/config
    sources:
      - "**/*.md"

{{#if hbs.python}}
  python:
    deps:
      - :common:python-requirements
    desc: Lint Python files using Flake8
    summary: |
      # Lint Python files using Flake8

      Flake8 is a Python library that lints Python projects (or projects that may include Python files).
      It is a combination of several Python linters like PyFlakes and pycodestyle. This task will run
      `flake8` using the configuration found in the `.flake8` file in the root of this project.

      The `.flake8` file is a common file shared across many of our repositories so if changes are made to
      it then the changes need to be made to the appropriate common file repository [here](https://gitlab.com/megabyte-labs/common).
      Because of this, it might make more sense to add a comment to lines that you wish to be ignored by flake8.
      For instance, you can ignore rule E234 by adding a comment at the end of the line that looks like, "# noqa: E234".

      _NOTE: In order to maintain our strict quality standards, disabling Flake8 rules should only be done
      when absolutely necessary._

      **Example scanning all files:**
      `task lint:python`

      **Example scanning specific file:**
      `task lint:python -- myfile.py`

      For more information, see [Flake8's GitHub page](https://github.com/PyCQA/flake8).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          flake8 .
        else
          flake8 \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*.py"

{{/if}}
  scripts:
    deps:
      - :npm:shellcheck
    desc: Report possible errors in shell scripts
    summary: |
      # Report possible errors in shell scripts using Shellcheck

      Shellcheck is a tool that reports warnings and suggestions for shell (e.g. bash) scripts. This
      task is a wrapper for the `npm run lint:shellcheck` command. The npm command scans the project
      for files ending with `.sh` or `.sh.j2` and runs Shellcheck on them. Files in the following folders
      are ignored:
        * .cache        * .husky
        * .git          * node_modules
        * .husky        * slim_test

      **Example scanning all files:**
      `task lint:scripts`

      **Example scanning specific file:**
      `task lint:scripts -- myfile.sh`

      For more information, see [Shellcheck's GitHub page](https://github.com/koalaman/shellcheck).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          find . -type d \( \{{.IGNORE_FOLDERS}} \) -prune -o -type f \( -name '*.sh' -o -name '*.sh.j2' \) -print0 |
            xargs -0 -r -n1 shellcheck
        else
          shellcheck \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*.(sh|sh.j2)"

  spelling:
    deps:
      - :npm:cspell
      - :software:git
      - :software:node
    desc: Checks for spelling errors in staged files
    summary: |
      # Check for spelling errors in staged files

      Use cspell to check for possible spelling errors using the configuration stored in `.common/.cspell.json`.

      This task is utilized by the pre-commit hook. For more information about cspell, see the
      [cspell NPM page](https://www.npmjs.com/package/cspell).

      **Example scanning all staged files:**
      `task lint:spelling`

      **Example scanning specific file:**
      `task lint:spelling -- myfile.sh`

      **Although this task only analyzes staged files, you can manually run cspell, for example, on all JavaScript
      files by running:**
      `npx cspell '**/*.js'`
    env:
      STAGED_FILES:
        sh: git diff --cached --name-only
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          cspell --no-summary --no-progress --show-context --no-must-find-files --config .common/.cspell.json "$STAGED_FILES"
        else
          cspell --no-progress --show-context --no-must-find-files --config .cspell.json \{{.CLI_ARGS}}
        fi
    preconditions:
      - sh: "git diff --cached --name-only"
        msg: "Cannot perform a spell check because there are no staged files"

{{#if hbs.vagrant}}
  vagrant:
    deps:
      - :software:vagrant
    desc: Validate the Vagrantfile
    summary: |
      # Validate the Vagrantfile

      This task is an alias for `vagrant validate`. Vagrant's `validate` command
      will ensure the Vagrantfile in the root of this repository has no errors and
      is using valid syntax.

      For more information on `vagrant validate`, see the [Vagrant website](https://www.vagrantup.com/docs/cli/validate).
    cmds:
      - vagrant validate
    sources:
      - Vagrantfile

{{/if}}
{{#if hbs.yaml}}
  yaml:
    deps:
      - :common:python-requirements
    desc: Lint .yml files using YAML Lint
    summary: |
      # Lint YML files using YAML Lint

      YAML Lint is a general purpose linter tool that reports suggestions for `.yml`
      files. It checks for syntax validity as well as cosmetic problems like line
      lengths, trailing spaces, and indentation. The configuration file is in the root
      of the repository in the file named `.yamllint`. The `.yamllint` file is shared
      across all of our projects so if you need to make changes to get rid of warnings
      it will generally make more sense to disable YAML Lint for a single line using the
      [method described here](https://github.com/adrienverge/yamllint#features).

      _NOTE: Disabling YAML Lint rules should only be done when absolutely necessary._

      **Example scanning all '**/*.yml' files:**
      `task lint:yaml`

      **Example scanning specific file:**
      `task lint:yaml -- myfile.sh`

      For more information, see the [YAML Lint GitHub page](https://github.com/adrienverge/yamllint).
    cmds:
      - |
        if [ -z "\{{.CLI_ARGS}}" ]; then
          yamllint -s .
        else
          yamllint -s \{{.CLI_ARGS}}
        fi
    sources:
      - "**/*.yml"

{{/if}}
